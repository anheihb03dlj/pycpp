# pycpp(Python和C++相互调用例子)

python与c++的混合开发分为2种：

（1）扩展extending  （2）嵌入embedding

目前PyScript项目两者都兼顾，在arx模块里嵌入一个python解释器，然后将C/C++的函数及类导出给python使用，利用python语法和模块编写一些命令和函数

1、python扩展方法

参考: 
    如何实现 C/C++ 与 Python 的通信？
    http://www.zhihu.com/question/23003213

    Python与C++联合编程的简介(cython、swig、boost python的简单对比)
    http://hgoldfish.com/blogs/article/87/

给python做扩展模块的方法很多,比如python c api、cython、swig、boost.python、pycxx等

2、boost python在arx项目中的编译选项设置
(1)boost python在arx的debug项目编译会报错,修改如下:
    vc项目属性-->C/C++-->代码生成-->基本运行时检查，改为"默认值"

(2)链接boost python的静态库
参考: http://www.cnblogs.com/hdtianfu/archive/2012/07/27/2611382.html
boost.python默认情况下链接的是动态库(名字是：boost_python-vc90-mt-gd-1_50.lib ) 
要链接boost python的静态库, 需要添加一个预处理宏： BOOST_PYTHON_STATIC_LIB

(3)编译boost python(ThirdParty/boost目录下我也编写了一个boost编译方法教程)
http://www.360doc.com/content/12/0328/18/9369336_198684277.shtml


3、c++多线程中嵌入python的解决方法

C++多线程中调用python api函数
http://blog.csdn.net/skyremember/article/details/2933616

c++多线程调用python
http://www.pythontip.com/blog/post/4745/

c++调用python函数
https://bfroehle.com/2011/07/18/boost-python-and-boost-function/
http://blog.csdn.net/xsloop/article/details/46970733

4、boost python的使用教程

boost.python库应用之嵌入python
http://blog.csdn.net/you_lan_hai/article/details/7919572

Boost.Python C++与Python的互相调用之Python调用c/c++函数
http://blog.csdn.net/yangdelong/article/details/4676413

boost.python入门教程 ----python 嵌入c++
http://www.cnblogs.com/rocketfan/archive/2009/11/15/1603400.html

Python实例浅谈之三Python与C/C++相互调用
http://blog.csdn.net/taiyang1987912/article/details/44779719

5、Py_Initialize崩溃的原因及解决方法
摘录一些稍微靠谱的答案：
(1) http://stackoverflow.com/questions/7565033/how-do-i-stop-py-initialise-crashing-the-application

the app is crashing because [Py_Initialise] does not found some folder or file?! 
so if you change to the folder where [Py_Initialise] search for the files/folders it will not crash 
解释:他认为Py_Initialise初始化的时候，在PythonHome目录下找不到DLLs和Lib文件夹(最好使用官方标准python目录,缺一个都不行)

(2) http://stackoverflow.com/questions/18428600/unable-to-get-python-embedded-to-work-with-zipd-library

Just as sys.path currently has default directory names, a default zip archive name is added too. 
Otherwise there is no way to import all Python library files from an archive
解释:如果想通过python27.zip的方式加载python标准库,名称是一个问题(但我还是没有找到正确的方法打包得到python27.zip)
      使用python27.zip要更方便一些,毕竟目录太多了(DLLs和Lib)

(3) http://stackoverflow.com/questions/6948779/missing-c-windows-system32-python27-zip-file

I believe your original problem is that you placed the folders in your python zip, 
when you should place the contents of the DLLs and Lib folders into the python zip.
Instead of this:

python27.zip
    DLLs
    *.*
    Lib
    *.*
You should have:

python27.zip
    *.* from DLLs
    *.* from Lib
解释: 他认为应该将DLLs和Lib中的所有文件复制出来,然后打包到python27.zip,而不是直接压缩DLLs和Lib(但是我测试了不好使!!)


(4) https://www.python.org/dev/peps/pep-0273/
Import Modules from Zip Archives
解释: 这是python语言规范,解释了通过zip压缩包加载python库的方法(也就是python27.zip,但没有说该怎么样打包!!!)

(5) http://python.6.x6.nabble.com/Embedding-Python-s-library-as-zip-file-td1645131.html#a1645140
    http://www.gossamer-threads.com/lists/python/python/916791

As far as I understand, beside of my executable and Python.dll (I am using 
Python27), I need to provide two folders: 
  - DLLs, 
  - Lib 

If I place the Lib folder and the contents of the DLLs folder in a directory 
of my executable, then everything seems to work. 
However I would like to use a zipped Lib folder. Hence I made an archive 
(which contains contents of Lib folder) called Python27.zip. Unfortunately 
the app crashes during execution. 
I assume the reason might be lack of zlib.pyd. Is that assumption correct? 
If so, how to obtain it? If not, what am I doing wrong? 
解释: 与我的做法类似,在将python官方安装包里的DLLs、Lib、python27.dll拷贝过来,可以正常运行,
    但是如果将DLLs和Lib直接压缩成python27.zip, Py_Initialize()会崩溃(说明python27.zip打包的方法不正确!!!)


6、Python C API的使用教程
C++中嵌入python程序——使用API接口，从函数到类
http://blog.csdn.net/yiyouxian/article/details/51993524

C++调Python示例
http://www.cnblogs.com/Hisin/archive/2012/02/27/2370590.html


7、cython使用教程
Cython的学习方法(这是一个系列的教程)
http://blog.csdn.net/I2Cbus/article/details/23791309

Cython学习
http://www.cnblogs.com/kaituorensheng/p/4452881.html

Cython官网的wiki(如何封装C++)
http://docs.cython.org/en/latest/src/userguide/wrapping_CPlusPlus.html

cython和swig的对比
https://github.com/chenbk85/cython_swig

github上的一些cython封装C++的代码
https://github.com/sturlamolden/cython-cpp-test/blob/master/pytestclass.pyx


# 项目代码说明

1、pycpp/CXX
这是PyCxx项目的源代码,因为代码不多,直接复制到项目下编译使用在ThirdParty/pycxx目录下，有完整的源代码src、文档doc、示例代码demo
官网在线文档: http://www.wellho.net/course/PyCXX.html

PyCXX严格来说并不是一个封装库，它可以看作是Python C API的面向对象版本，
将Python C API的一些数据类型进行了封装，稍微简化了函数、类的封装，避免了一些繁琐的Python C API操作及问题
比如PyObject对象的引用计数问题

这个库也是通过freecad才知道的，freecad里也少量的使用了PyCxx封装了一些C++类和函数

2、FreeCAD移植过来的代码
包括：
    PyTools.h/PyTools.c(对Python C API进行了一定的封装,比如加载模块、调用函数、异常处理等)
    Interpreter.h/Interpreter.cpp(封装了一个Python解释器类,提供了很多非常有用的函数)
    Exception.h/Exception.cpp(定义了异常类、GIL处理、线程处理等工具,也很有用)

这3对文件提供了非常有用的函数,我认为它们实现的功能足够使用了，也不比boost python弱到哪儿去，相反的更加灵活一些!

FreeCAD里的C++类扩展使用了一套相对复杂的自动化工具，需要编写一个xml文件，描述类的属性、方法等，然后通过自动化脚本(python写的)自动生成C++类以及Python封装。
比如Axis类,包括:
    Axis.h/Axis.cpp (纯C++实现,与python无关)
    Axis.xml (人工编写xml文件,描述要导出的AxisPy类的属性和方法)
    AxisPy.h/AxisPy.cpp (自动生成的封装类AxisPy,导出Python使用,这2个文件完成了绝大多数的python扩展实现代码)
    AxisPyImp.cpp (人工编写, AxisPy类的方法实现,需要调用Axis.h里的类方法)

Boost Python最大的缺点是编译速度太慢,文件尺寸较大,所以FreeCAD采用了自己编写的Python封装库

4、Boost Python
目前只考虑封装函数和类的时候boost python，嵌入Python解释器的时候使用FreeCAD代码以及PyCXX代码