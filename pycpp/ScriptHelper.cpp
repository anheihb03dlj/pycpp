#include "StdAfx.h"
#include "ScriptHelper.h"
#include "Interpreter.h"
#include "PyObjectBase.h"
#include "ArxHelper/HelperClass.h"
#include "Tool/HelperClass.h"
#include <boost/python.hpp>
static void PrintPythonPath()
{
    PyGILStateLocker lock;
    // (1)直接通过Python C API获取PYTHONPATH
    //const char* sPath = Py_GetPath();
    //acutPrintf(_T("\n%s"), C2W(sPath));
    using namespace boost;
    // (2)或利用boost python执行python语句获取PYTHONPATH
    // Load the sys module.
    python::object sys = python::import( "sys" );
    // Extract the python version.
    python::list path_list = python::extract<python::list>( sys.attr( "path" ) );
    for ( int i = 0; i < python::len( path_list ); ++i )
    {
        std::string sPath = python::extract<std::string>( path_list[i] );
        acutPrintf( _T( "\n%s" ), C2W( sPath.c_str() ) );
    }
}
// 拆分模块名和函数名
static void SplitFuncName( const std::string& name, std::string& module, std::string& func )
{
    if( name == "" ) return;
    int nPos = -1;
    for( int i = ( int )name.size() - 1; i >= 0; --i )
    {
        char cChar = name[i];
        if ( '.' == cChar )
        {
            nPos = i;
            break;
        }
    }
    if( nPos == -1 )
    {
        func = name;
    }
    else
    {
        module = name.substr( 0, nPos );
        func = name.substr( nPos + 1, name.size() - nPos - 1 );
    }
}
// 在arx路径下查找脚本文件并执行
static bool RunRelativeFile1( const CString& file )
{
    CString dir = ARX_FILE_PATH( _T( "" ) );
    // 在arx根目录下查找脚本文件
    CString script_file = PathHelper::BuildPath( dir, file );
    acutPrintf( _T( "\n执行脚本:%s" ), script_file );
    if( PathHelper::IsFileExist( script_file ) )
    {
        Interpreter::getSingletonPtr()->runFile( W2C( ( LPCTSTR )script_file ), false );
        return true;
    }
    else
    {
        return false;
    }
}
// dwg文件所在路径下查找脚本文件并执行
static bool RunRelativeFile2( const CString& file )
{
    CString dir = ArxDwgHelper::GetDwgDirectory( curDoc() );
    // dwg文件所在路径下下查找脚本文件
    CString script_file = PathHelper::BuildPath( dir, file );
    acutPrintf( _T( "\n执行脚本:%s" ), script_file );
    if( PathHelper::IsFileExist( script_file ) )
    {
        Interpreter::getSingletonPtr()->runFile( W2C( ( LPCTSTR )script_file ), false );
        return true;
    }
    else
    {
        return false;
    }
}
static bool RunAbsoluteFile( const CString& file )
{
    acutPrintf( _T( "\n执行脚本:%s" ), file );
    Interpreter::getSingletonPtr()->runFile( W2C( ( LPCTSTR )file ), false );
    return true;
}
void ScriptHelper::Run( const CString& script )
{
    PrintPythonPath();
    PyGILStateLocker lock;
    try
    {
        CString dir = PathHelper::GetDirectory( script );
        // file是一个文件名，不包含路径
        if( dir == script )
        {
            if( ArxDwgHelper::IsNewDwg( curDoc() ) )
            {
                // 在arx路径下查找脚本文件并执行
                RunRelativeFile1( script );
            }
            else
            {
                // dwg文件所在路径下查找脚本文件并执行
                if( !RunRelativeFile2( script ) )
                {
                    // 在arx路径下查找脚本文件并执行
                    RunRelativeFile1( script );
                }
            }
        }
        // file是一个绝对路径
        else
        {
            RunAbsoluteFile( script );
        }
    }
    catch( Exception& e )
    {
        std::string str;
        str += "base exception thrown (";
        str += e.what();
        str += ")";
        e.ReportException();
        PyErr_SetString( BaseExceptionFreeCADError, str.c_str() );
    }
    catch( std::exception& e )
    {
        std::string str;
        str += "std exception thrown (";
        str += e.what();
        str += ")";
        acutPrintf( C2W( str.c_str() ) );
        PyErr_SetString( BaseExceptionFreeCADError, str.c_str() );
    }
    catch( const Py::Exception& )
    {
        PyException::Report();
    }
    catch( const char* e )
    {
        PyErr_SetString( BaseExceptionFreeCADError, e );
        PyException::Report();
    }
    catch( ... )
    {
        PyErr_SetString( BaseExceptionFreeCADError, "Unknown C++ exception" );
        PyException::Report();
    }
}
// 从文件对话框中选择db文件
bool ScriptHelper::GetScriptFromDialog( CString& script )
{
    CFileDialog openDialog(
        TRUE,
        _T( "py" ),
        NULL, OFN_HIDEREADONLY | OFN_FILEMUSTEXIST,
        _T( "脚本文件(*.py)|*.py||" ) );
    // 设置对话框的默认路径
    CString initDir = ArxDwgHelper::GetDwgDirectory( curDoc() );
    if( ArxDwgHelper::IsNewDwg( curDoc() ) )
    {
        initDir = ARX_FILE_PATH( _T( "" ) );
    }
    // 不能直接将CString赋给lpstrInitialDir
    // lpstrInitialDir要求一个以\0结尾的字符串!!!!要使用GetBuffer()方法
    openDialog.m_ofn.lpstrInitialDir = initDir.GetBuffer();
    // 对话框标题
    openDialog.m_ofn.lpstrTitle = _T( "请选择Py脚本" );
    if( IDOK == openDialog.DoModal() )
    {
        script = openDialog.GetPathName();
        initDir.ReleaseBuffer();
        return true;
    }
    else
    {
        return false;
    }
}
void ScriptHelper::test1()
{
    PrintPythonPath();
    PyGILStateLocker lock;
    using namespace boost;
    python::object main_module = python::import( "__main__" );
    python::object main_namespace = main_module.attr( "__dict__" );
    // 采用import模块的方法倒是好使!
    python::object hello_module = python::import( "hello" );
    python::object hello_namespace = hello_module.attr( "__dict__" );
    int a = python::extract<int>( hello_namespace["a"] );
    int b = python::extract<int>( hello_namespace["b"] );
    int c = python::extract<int>( hello_namespace["c"] );
    // 目前直接运行文件会产生异常，原因未知!!!!
    //CString py_file = ARX_FILE_PATH( _T( "hello.py" ) );
    //python::object hello = python::exec_file(W2C((LPCTSTR)py_file),main_namespace, main_namespace);
    //int a = python::extract<int>(main_namespace["a"]);
    //int b = python::extract<int>(main_namespace["b"]);
    //int c = python::extract<int>(main_namespace["c"]);
    acutPrintf( _T( "\na=%d  b=%d  c=%d" ), a, b, b );
}
void ScriptHelper::test2()
{
    PyGILStateLocker lock;
    using namespace boost;
    python::object main_module = python::import( "__main__" );
    python::object main_namespace = main_module.attr( "__dict__" );
    python::exec( "hello = file('hello.txt', 'w')\n"
                  "hello.write('Hello world!')\n"
                  "hello.close()",
                  main_namespace );
    python::exec( "result = 5 ** 2", main_namespace );
    int five_squared = python::extract<int>( main_namespace["result"] );
    //cout << "The five_squeared caculated by python is " << five_squared << endl;
    // Load the sys module.
    python::object sys = python::import( "sys" );
    // Extract the python version.
    std::string version = python::extract<std::string>( sys.attr( "version" ) );
    //std::cout << version << std::endl;
    //要求simple.py与可执行文件在相同路径下! 运行ok
    python::object simple = python::exec_file( "simple.py", main_namespace, main_namespace );
    //dict global;
    //object result = exec_file("simple.py", global, global);
    python::object foo = main_namespace["foo"];
    int val = python::extract<int>( foo( 5 ) );
    //cout << "Python has caculated foo as " << val << endl;
}
void run( const char* py_file )
{
    PyGILStateLocker lock;
    using namespace boost;
    python::object main_module = python::import( "__main__" );
    python::object main_namespace = main_module.attr( "__dict__" );
    python::object simple = python::exec_file( py_file, main_namespace, main_namespace );
}
void call( const char* func )
{
    PyGILStateLocker lock;
    using namespace boost;
    // 拆分模块名和函数名
    std::string sModule, sFunc;
    SplitFuncName( func, sModule, sFunc );
    python::object func_module = python::import( "__main__" );
    if( !sModule.empty() )
    {
        // 加载模块
        func_module = python::import( sModule.c_str() );
    }
    python::object module_namespace = func_module.attr( "__dict__" );
    python::object module_func = module_namespace.attr( sFunc.c_str() );
    // 执行模块中的函数
    module_func();
}
