#pragma once
#include "dlimexp.h"
// todo: 该类还没有考虑好要做啥....本意是想提供给其它外部项目使用
class PY_EXPORT_API ScriptHelper
{
public:
    // 执行py脚本
    static void Run( const CString& script );
    // 通过对话框选择py脚本
    static bool GetScriptFromDialog( CString& script );
    static void test1();
    static void test2();
};